# Floating point arithmetic exercises

## 1. Find the machine $`\,\epsilon\,`$ for various FP datatypes

The machine $`\epsilon`$ is a property of the FP datatype (and implementation) and it is the  smallest FP-valid  number that can be summed to $`1`$ satisfying
```math
(*) \quad 1 + \epsilon > 1.
```

Find iteratively the value of $`epsilon`$ for different FP datatypes (`np.float16`,`np.float32`,`np.float64`,`np.float128`).

**Hint:** start setting $`\epsilon = 1`$. Reduce $`\epsilon`$ by a factor $`2`$ when $`(*)`$  is true.

**Note:**  $`1`$ is a valid FP number. Halving it yields valid FP numbers. Why?

## 2. Binary FP string to float & Test-Driven-Development

In normalized scientific notation and base 2, a number is written as
```math
x = \pm 0.a_1a_2\ldots a_k \cdot 2^{\pm b_hb_{h-1}\ldots b_0}
```
where $`a_j,b_j \in \{0,1\}`$ and $`a_1 = 1`$. 

The previous is a shorthand and conventional notation for 
```math
x = \pm\sum_j a_j \cdot 2^{-j + E}
```
where 
```math
E = \pm \sum_{j = 0}^h b_j \cdot 2^{j}
```

* Write a program that from command line accepts a binary number in scientific and normalized form and prints its representation in base 10 floating point.
    
    ```bash
    python convert.py 0.101e1
    1.25
    ```
Follow the template in `binary_str_2_float/converter.py`
* Make sure that it satisfies all the requirements by testing it `nosetests -v` (9 tests in total -- `pip install nose` to install).
* What's your final coverage?
* Can you achieve 100% coverage?
* Challenge: can you have the math part fully numpy-vectorized?

## 3. Cancellation error when computing discrete derivatives

The 1st order (forward) finite difference of a function $`f`$ at location  $`x`$ with increment $`h`$ reads

```python
def FD(f, x, h):
    return (f(x+h)-f(x))/h
```

As $`h`$ gets smaller and smaller the finite difference tends to the first derivative of $`f`$ in $`x`$.

You can compare the tangent line to a sine function based on the actual and on the approximate derivative as follows.

``` python
%matplotlib inline
import matplotlib.pyplot as plt

x = np.linspace(.5,1.5)
y = np.sin(x)

analytical_slope = np.cos(1)

numerical_slope = FD(np.sin, 1, .4)

plt.plot(x,y)
plt.plot(x,analytical_slope*(x-1)+np.sin(1))
plt.plot(x,numerical_slope*(x-1)+np.sin(1))
plt.legend(['courve','analytical tangent','numerical tangent'],loc=4)

plt.show()
```


**ISSUE:** as $`h`$ gets small $`f(x+h)\approx f(x)`$ which yields numerical cancellation. Using a 64bit FP arithmetic plot the error

```math
E = |FP(f,x,h) - cos(x)|
```
at $`x=1`$ for $`h`$ decreasing in order of magnitude. (make a log-log plot).

* What is the trend of the error?
* Where does it stop to be monotonically decreasing?
* Why?




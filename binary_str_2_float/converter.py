from __future__ import print_function
from functools  import reduce
import numpy as np
def binary_number_string_parser(inp
             , datatype = float):

    if inp[0] not in ["+","-"]:
        ninp = "+"
        ninp +=  (inp)
        inp = ninp

    assert inp[1] == "0"
    assert inp[2] == "."
    assert "e" in inp
    after_zero = inp.split('.')[1]
    mantissa = after_zero.split("e")[0]
    exponent = after_zero.split("e")[1]
    
    print ("mantissa ->", mantissa)
    print ("exponent ->", exponent)

    if mantissa[0] != "1":
        raise "Expected normalized representation"

    exponent_sign = 1.
    if exponent.startswith('+'):
        exponent = exponent[1:]
    elif exponent.startswith('-'):
        exponent = exponent[1:]
        exponent_sign = -1.

       
    exponent_val = exponent_sign * sum(list(
        [datatype(x) * 2** idx  for idx,x in enumerate(exponent[::-1])
        ]  ))
    
    val = list([datatype(x) * 2**(-(idx + 1) + exponent_val ) for idx,x in enumerate(mantissa)])

    print (val)
    end_res = sum(val[::-1]) * (-1. if inp[0] == '-' else +1.) 
    print (end_res)


    trivial_sum = 0.    
    for v in val:
        #print v
        trivial_sum += v
    trivial_sum *= (-1. if inp[0] == '-' else +1.) 
    print (trivial_sum)

    assert reduce(lambda x,y : x+y, val[::-1],0.) == reduce(lambda x,y : x+y, val[::],0.)
    assert sum(val[::-1]) == sum(val)
    return end_res

def main():
    import sys
    print ("input", sys.argv[1])
    return binary_number_string_parser(sys.argv[1])


if __name__ == '__main__':
    main()
    
